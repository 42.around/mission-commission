const fs = require('fs');
const path = require('path');

const commissions = require('./core/commissions');
const { User } = require('./core/user');
const { Transaction } = require('./core/transaction');

const { argv } = process;
const DEFAULT_DATA_FILE = path.join(__dirname, 'test', 'data', 'input.json');
const DATA_FILES = argv.length > 2 ? argv.slice(2) : [DEFAULT_DATA_FILE];

const USERS = {};

const loadDataFile = (filePath) => (
  new Promise((resolve, reject) => {
    const fileHandle = (error, data) => {
      if (error) return reject(error);

      try {
        return resolve(JSON.parse(data));
      } catch (parsingError) {
        return reject(parsingError);
      }
    };
    fs.readFile(filePath, fileHandle);
  })
);

const getUser = (transaction) => {
  const id = String(transaction.user_id);
  if (!(id in USERS)) {
    USERS[id] = new User(id, transaction.user_type);
  }
  return USERS[id];
};

const executeInstruction = (instruction, configs) => {
  const user = getUser(instruction);
  const transaction = new Transaction(instruction, user, configs);
  user.transactions.push(transaction);
  return transaction.commission;
};

const calculateFileCommissions = (file, configs) => (
  new Promise((resolve, reject) => {
    loadDataFile(file).then((instructions) => {
      try {
        const instructionsCommissions = instructions.map((instruction) => (
          executeInstruction(instruction, configs)
        ));
        return resolve(instructionsCommissions);
      } catch (error) {
        return reject(error);
      }
    }, ({ message }) => {
      console.error(message); // eslint-disable-line no-console
      process.exit(8);
    });
  }));

commissions.load().then((configs) => {
  DATA_FILES.forEach((file) => {
    calculateFileCommissions(file, configs).then((output) => {
      // eslint-disable-next-line no-console
      output.forEach((result) => { console.log(result); });
    }).catch(() => {
      // eslint-disable-next-line no-console
      console.error('Cannot execute instructions because of compatibility issues');
      process.exit(1);
    });
  });
});
