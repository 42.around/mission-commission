# Mission commission

Node.js(ES6) code example.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

Node.js and `npm` are required to use this project. You can find more information official websites, listed in [Built With section](#built-with);

### Installing

After cloning this project, simply run

```
npm install
```

And project should download all required dependencies. You can test if it installed correctly by running main program:

```
node app.js
```

## Running the tests

You can execute all project tests by running
```
npm test
npm run test-unit
```

### Main test

Main test is responsible for maintaining consistent code style and ensuring that project is running without issues.

If you want to check code style only, run

```
npx eslint app.js core/*
```


### Unit tests

Unit tests are required to determine whether individual units of code are fit for use. You can launch unit tests by running

```
npm run test-unit
```

or

```
mocha test/unit/*.spec.js
```

Unit tests could be launched individually for each code file, for example `core/transaction.js` could be tested with command

```
mocha test/unit/transaction.spec.js
```

## Built With

* [Node.js](https://nodejs.org/) - Runtime environment
* [npm](https://www.npmjs.com/) - Dependency Management

## Authors

* **Tomas Markevičius** - *Initial work* - [Tomas Markevičius](https://gitlab.com/42.around)

## License

This project is licensed under the GPLv3.0 License - see the [LICENSE.md](LICENSE.md) file for details
