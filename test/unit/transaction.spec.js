const should = require('chai').should();

const commissions = require('./data/commissions');
const { convertToEur } = require('../../core/commissions');
const { Transaction } = require('../../core/transaction');
const { User } = require('../../core/user');

const USERS = {
  N: new User(42, 'natural'),
  J: new User(2, 'juridical'),
};

describe('Transaction class, core/transaction.js', () => {
  describe('Transaction initialization', () => {
    const config = {
      date: '2020-01-01',
      type: 'cash_in',
      operation: { amount: 42, currency: 'EUR' },
    };
    const transaction = new Transaction(config, USERS.N, commissions);

    it('should store date as Date object', () => {
      transaction.date.constructor.name.should.be.equal('Date');
    });

    it('should store week as a number', () => {
      transaction.week.should.be.a('number');
    });

    it('should store transaction type', () => {
      transaction.type.should.be.equal(config.type);
    });

    it('should convert to EUR and store operation amount as a number', () => {
      transaction.amountEur.should.be.a('number');
      transaction.amountEur.should.be.equal(convertToEur(config.operation));
    });

    it('should store user', () => {
      transaction.user.should.be.equal(USERS.N);
    });

    it('should store commission as a number', () => {
      transaction.commission.should.be.a('number');
    });
  });

  describe('Commission calculation', () => {
    describe('Cash in for all user types', () => {
      const commission = commissions.find(({ url }) => url === '/cash-in');
      const config = {
        date: '2020-01-01',
        type: 'cash_in',
        operation: { amount: 42, currency: 'EUR' },
      };
      const transactionN = new Transaction(config, USERS.N, commissions);
      const transactionJ = new Transaction(config, USERS.J, commissions);

      it('should charge commission fee (rounded up to nearest cents)', () => {
        const fee = transactionN.amountEur * commission.config.percents;
        transactionN.commission.should.be.equal(Math.ceil(fee) / 100);
      });

      it('should not charge more than certain amount for transaction', () => {
        const limitEur = convertToEur(commission.config.max);
        const largeTransaction = new Transaction(
          { ...config, operation: { amount: Infinity, currency: 'EUR' } },
          USERS.N, commissions,
        );

        largeTransaction.commission.should.be.equal(limitEur);
      });

      it('natural and juridical users should be charged the same', () => {
        transactionN.commission.should.be.equal(transactionJ.commission);
      });
    });

    describe('Cash out for natural user type', () => {
      const commission = commissions.find(
        ({ url }) => url === '/cash-out/natural',
      );
      const config = {
        date: '2020-01-01',
        type: 'cash_out',
        operation: commission.config.week_limit,
      };
      const transaction = new Transaction(config, USERS.N, commissions);
      USERS.N.transactions.push(transaction);

      it('should not charge until week limit is reached', () => {
        transaction.commission.should.be.equal(0);
      });

      it('should charge commission fee when limit is surpassed', () => {
        const newTransaction = new Transaction(config, USERS.N, commissions);
        const fee = newTransaction.amountEur * commission.config.percents;
        newTransaction.commission.should.be.equal(Math.ceil(fee) / 100);
      });

      it('week limit should reset every week', () => {
        const newTransaction = new Transaction(
          { ...config, date: '2020-01-08' }, USERS.N, commissions,
        );

        newTransaction.commission.should.be.equal(0);
      });
    });

    describe('Cash out for juridical user type', () => {
      const commission = commissions.find(
        ({ url }) => url === '/cash-out/juridical',
      );
      const config = {
        date: '2020-01-01',
        type: 'cash_out',
        operation: { amount: 1000, currency: 'EUR' },
      };
      const transaction = new Transaction(config, USERS.J, commissions);

      it('should charge commission fee (rounded up to nearest cents)', () => {
        const fee = transaction.amountEur * commission.config.percents;
        transaction.commission.should.be.equal(Math.ceil(fee) / 100);
      });

      it('should not charge less than certain amount for transaction', () => {
        const limitEur = convertToEur(commission.config.min);
        const smallTransaction = new Transaction(
          { ...config, operation: { amount: 5, currency: 'EUR' } },
          USERS.J, commissions,
        );

        smallTransaction.commission.should.be.equal(limitEur);
      });
    });
  });
});
