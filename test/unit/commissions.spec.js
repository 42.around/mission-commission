const should = require('chai').should();
const commissions = require('../../core/commissions');

describe('Commissions utilities, core/commissions.js', () => {
  describe('Conversion to EUR', () => {
    it('should convert EUR to EUR', () => {
      commissions.convertToEur({ amount: 42, currency: 'EUR' })
        .should.be.equal(42);
    });
  });

  describe('Commissions configurations loading', () => {
    let configs;
    before((done) => {
      commissions.load().then((loadedConfigs) => {
        configs = loadedConfigs;
        done();
      });
    });

    it('configurations should be stored in array', () => {
      configs.should.be.an('array');
    });

    it('configurations array should be consisted of objects', (done) => {
      configs.forEach((config) => config.should.be.an('object'));
      done();
    });

    it('configurations should contain regex string', (done) => {
      configs.forEach((config) => {
        config.should.have.property('regex').that.is.a('string');
      });
      done();
    });

    it('configurations should contain url string', (done) => {
      configs.forEach((config) => {
        config.should.have.property('url').that.is.a('string');
      });
      done();
    });

    it('configurations should contain config object', (done) => {
      configs.forEach((config) => {
        config.should.have.property('config').that.is.an('object');
      });
      done();
    });
  });
});
