const should = require('chai').should();
const { User } = require('../../core/user');

const DEFAULT_ID = 42;
const DEFAULT_TYPE = 'natural';

describe('User class, core/user.js', () => {
  const user = new User(DEFAULT_ID, DEFAULT_TYPE);

  describe('User initialization', () => {
    it('should store user id as string', () => {
      user.id.should.be.a('string');
      user.id.should.be.equal(String(DEFAULT_ID));
    });

    it('should store user type', () => {
      user.type.should.be.equal(DEFAULT_TYPE);
    });

    it('should store transactions list', () => {
      user.transactions.should.be.an('array');
    });
  });
});
