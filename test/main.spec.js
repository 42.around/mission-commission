const should = require('chai').should();
const { exec } = require('child_process');

const APP = `node ${__dirname}/../app.js`;
const DATA_DIR = `${__dirname}/data`;
const DATA = {
  not_existing: 'something/that/clearly/does/not/exist',
  incorrect_instructions: `${DATA_DIR}/incorrect_instructions.json`,
  main: `${DATA_DIR}/input.json`,
};

describe('Main program, app.js', () => {
  describe('loading file that does not exist', () => {
    it('should return exit code 8', (done) => {
      exec(`${APP} ${DATA.not_existing}`, ({ code }) => {
        code.should.be.equal(8);
        done();
      });
    });
  });

  describe('loading file that has incorrect instructions', () => {
    it('should return exit code 1', (done) => {
      exec(`${APP} ${DATA.incorrect_instructions}`, ({ code }) => {
        code.should.be.equal(1);
        done();
      });
    });
  });

  describe('loading main instructions', () => {
    let result;
    before((done) => {
      exec(`${APP} ${DATA.main}`, (error, stdout) => {
        result = { error, stdout };
        done();
      });
    });

    it('return code should be 0', () => {
      should.not.exist(result.error);
    });

    it('stdout should match control data', () => {
      result.stdout.should.be.equal('0.06\n0.9\n87\n3\n0.3\n0.3\n5\n0\n0\n');
    });
  });
});
