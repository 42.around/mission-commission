const https = require('https');

const EXCHANGE_RATES = {
  EUR: 1,
};

const CONFIGS_ROOT_URL = 'https://private-38e18c-uzduotis.apiary-mock.com/config/';

const CONFIGS = [
  {
    regex: '^/cash_in',
    url: '/cash-in',
  }, {
    regex: '^/cash_out/natural',
    url: '/cash-out/natural',
  }, {
    regex: '^/cash_out/juridical',
    url: '/cash-out/juridical',
  },
];

const getHttpJson = (url) => (new Promise((resolve, reject) => {
  https.get(url, (response) => {
    const data = [];

    response.on('data', (chunk) => { data.push(chunk); });
    response.on('end', () => { resolve(JSON.parse(data)); });
  }).on('error', (error) => reject(error));
}));

const loadConfig = (commission) => (new Promise((resolve) => {
  getHttpJson(`${CONFIGS_ROOT_URL}${commission.url}`)
    .then((config) => { resolve({ ...commission, config }); });
}));

module.exports.convertToEur = ({ amount, currency }) => (
  amount * EXCHANGE_RATES[currency]);

module.exports.load = () => (Promise.all(
  CONFIGS.map((config) => (
    new Promise((resolve) => { loadConfig(config).then(resolve); })
  )),
));
