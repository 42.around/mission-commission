class User {
  constructor(id, type) {
    this.id = String(id);
    this.type = type;
    this.transactions = [];
  }
}

module.exports.User = User;
