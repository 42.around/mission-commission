const { convertToEur } = require('./commissions');

const JAN_ONE = new Date(0); // Start of UNIX time, 1970-01-01
const DAY = 86400000;

const calculateCommission = (transaction, {
  min, max, week_limit, percents, // eslint-disable-line camelcase
}) => {
  const ratio = percents / 100;
  let commission = 0;

  if (min) {
    commission += Math.max(ratio * transaction.amountEur, convertToEur(min));
  }

  if (max) {
    commission += Math.min(ratio * transaction.amountEur, convertToEur(max));
  }

  if (week_limit) { // eslint-disable-line camelcase
    const weekTransactionsAmounts = transaction.user.transactions
      .filter(({ week }) => (week === transaction.week))
      .filter(({ type }) => (type === transaction.type))
      .map(({ amountEur }) => (amountEur));

    const weekTotalEur = weekTransactionsAmounts.length === 0 ? 0 : (
      weekTransactionsAmounts.reduce((sum, value) => (sum + value))
    );

    commission += ratio * Math.max(0,
      (transaction.amountEur - Math.max(
        0, convertToEur(week_limit) - weekTotalEur,
      )));
  }

  return Math.ceil(commission * 100) / 100;
};

class Transaction {
  constructor({ date, type, operation }, user, commissions) {
    this.date = new Date(date);
    this.week = Math.ceil(
      ((this.date - JAN_ONE) / DAY + JAN_ONE.getDay()) / 7,
    );
    this.type = type;
    this.amountEur = convertToEur(operation);
    this.user = user;

    this.commission = calculateCommission(this,
      commissions.find(({ regex }) => this.match(regex)).config);
  }

  match(regex) { return `/${this.type}/${this.user.type}`.match(regex); }
}

module.exports.Transaction = Transaction;
